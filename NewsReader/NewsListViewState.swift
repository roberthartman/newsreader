//
//  NewsListViewState.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

struct NewsListViewState {
    let items: [Item]

    struct Item {
        let url: String
        let title: String
        let description: String?
        let imageUrl: String?
    }
}

extension NewsListViewState {
    static func fromNewsFetchResult(_ result: NewsFetchResult) -> NewsListViewState {
        NewsListViewState(
            items: result.articles.map {
                Item(
                    url: $0.url,
                    title: $0.title,
                    description: $0.description,
                    imageUrl: $0.urlToImage
                )
            }
        )
    }
}
