//
//  NewsFetcherImpl.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

struct NewsFetcherImpl: NewsFetcher {
    let url: URL

    func fetch(completion: @escaping (NewsFetchResult) -> Void) {
        print("fetching...")
        URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
            guard error == nil else {
                print("\(error!)")
                return
            }

            guard let data = data  else {
                print("Error: \(error?.localizedDescription ?? "Unknown error")")
                return
            }

            switch NewsFetchResult.decode(from: data) {
            case .failure(let error):
                print(error)
            case .success(let result):
                print("Done fetching: \(result.articles.count) results")
                DispatchQueue.main.async {
                    completion(result)
                }
            }
        }.resume()
    }
}
