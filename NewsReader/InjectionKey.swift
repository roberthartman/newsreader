//
//  InjectionKey.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

// Taken from https://www.avanderlee.com/swift/dependency-injection/

public protocol InjectionKey {

    /// The associated type representing the type of the dependency injection key's value.
    associatedtype Value

    /// The default value for the dependency injection key.
    static var currentValue: Self.Value { get set }
}
