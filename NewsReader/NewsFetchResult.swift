//
//  NewsFetchResult.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

struct NewsFetchResult: Codable {
    var articles: [Article]
}
