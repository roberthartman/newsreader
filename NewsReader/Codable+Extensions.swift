//
//  Model.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

extension Encodable {
    func encode() -> Result<Data, Error> {
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            return try .success(encoder.encode(self))
        } catch let e {
            print(e)
            return .failure(e)
        }
    }
}

extension Decodable {
    static func decode(from data: Data?) -> Result<Self, Error> {
        guard let data = data else {
            return .failure(NSError())
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601

        do {
            return try .success(decoder.decode(Self.self, from: data))
        } catch {
            print("Failed to decode: \(String(data: data, encoding: .utf8) ?? "na"): \(error)")
            return .failure(error)
        }
    }
}
