//
//  MockNewsFetcherImpl.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

struct MockNewsFetcherImpl: NewsFetcher {
    func fetch(completion: @escaping (NewsFetchResult) -> Void) {
        print("Not gonna do it, I'm only a mock.")
    }
}
