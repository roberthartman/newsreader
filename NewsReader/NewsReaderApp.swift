//
//  NewsReaderApp.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import SwiftUI

@main
struct NewsReaderApp: App {
    var body: some Scene {
        WindowGroup {
            NewsListView()
        }
    }
}
