//
//  NewsFetcher.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

protocol NewsFetcher {
    func fetch(completion: @escaping (NewsFetchResult) -> Void)
}
