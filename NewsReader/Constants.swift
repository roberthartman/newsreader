//
//  Constants.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

struct Constants {
    private static let newsServer = "newsapi.org/v2"
    private static let apiKey = "73c1fa40457c4ef2b747ffec4fd1da70"
    private static let country = "us"
    private static let newsUrlString = "https://\(newsServer)/top-headlines?country=\(country)&apiKey=\(apiKey)"
    static let newsUrl = URL(string: newsUrlString)!
}
