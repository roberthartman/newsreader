//
//  NewsArticleView.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
    let request: URLRequest

    func makeUIView(context: Context) -> WKWebView  {
        return WKWebView()
    }

    func updateUIView(_ uiView: WKWebView,  context: Context) {
        uiView.load(request)
    }
}

struct NewsArticleView: View {
    let url: String
    let title: String?

    var body: some View {
        WebView(
            request: URLRequest(url: URL(string:url)!)
        )
        .navigationBarTitle(title ?? "News Details")
    }
}

struct NewsView_Previews: PreviewProvider {
    static var previews: some View {
        NewsArticleView(url: "https://codemag.com/Magazine", title: nil)
    }
}
