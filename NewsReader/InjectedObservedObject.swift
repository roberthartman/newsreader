//
//  InjectedObservedObject.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import SwiftUI

/// For when you want to inject (@Injected) something that is also observable
/// (@ObservedObject), like a View Model.
/// This comes from: https://stackoverflow.com/a/64776324/5471368
/// and https://www.avanderlee.com/swift/dependency-injection/
@propertyWrapper
public struct InjectedObservedObject<T>: DynamicProperty where T: ObservableObject {
    private let keyPath: WritableKeyPath<InjectedValues, T>

    @ObservedObject private var currentValue: T

    init(_ keyPath: WritableKeyPath<InjectedValues, T>) {
        self.keyPath = keyPath
        self.currentValue = InjectedValues[keyPath]
    }

    public var wrappedValue: T {
        get { InjectedValues[keyPath] }
        set {
            InjectedValues[keyPath] = newValue
            currentValue = newValue
        }
    }
}
