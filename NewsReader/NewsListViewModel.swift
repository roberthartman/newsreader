//
//  NewsListViewModel.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

class NewsListViewModel: ObservableObject {
    @Published private(set) var state = NewsListViewState(items: [])

    @Injected(\.newsFetcher) private var fetcher: NewsFetcher

    func refresh() {
        fetchTheNews()
        switchFetchers()
    }

    private func fetchTheNews() {
        fetcher.fetch { [weak self] in
            self?.state = NewsListViewState.fromNewsFetchResult($0)
        }
    }

    // Demonstrates changing the dependency on the fly. Maybe useful for unit testing.
    private func switchFetchers() {
        if InjectedValues[\.newsFetcher] is NewsFetcherImpl {
            InjectedValues[\.newsFetcher] = MockNewsFetcherImpl()
        } else {
            InjectedValues[\.newsFetcher] = NewsFetcherImpl(url: Constants.newsUrl)
        }
    }
}
