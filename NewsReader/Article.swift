//
//  Article.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

struct Article: Codable {
    var url: String
    var title: String
    var description: String?
    var urlToImage: String?
}
