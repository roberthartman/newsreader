//
//  NewsListView.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import SwiftUI
import URLImage

struct NewsListView: View {
    @InjectedObservedObject(\.newsListViewModel)
    private var viewModel: NewsListViewModel

    var body: some View {
        NavigationView {
            List(viewModel.state.items, id: \.url) { item in
                let articleView = NewsArticleView(url: item.url, title: item.title)

                NavigationLink(destination: articleView) {
                    buildImage(url: item.imageUrl)
                    buildText(title: item.title, description: item.description)
                }
            }
            .onAppear(perform: refresh)
            .navigationBarTitle("Top Headlines", displayMode: .inline)//.truncationMode(.tail)
            .toolbar {
                Button("Refresh") {
                    refresh()
                }
            }
        }
    }

    private func buildImage(url: String?) -> some View {
        HStack(alignment: .top) {
            URLImage(URL(string: url ?? "https://picsum.photos/100")!) {
                // This view is displayed before download starts
                EmptyView()
            } inProgress: { progress in
                // Display progress
                Text("Loading...")
            } failure: { error, retry in
                // Display error and retry button
                VStack {
                    Text(error.localizedDescription)
                    Button("Retry", action: retry)
                }
            } content: {
                $0.resizable()
                    .aspectRatio(contentMode:.fit)
                    .clipped()
            }.frame(width: 80.0, height: 80.0)
        }
    }

    private func buildText(title: String, description: String?) -> some View {
        VStack(alignment: .leading) {
            Text(title).font(.headline)
            Text(description ?? "").font(.footnote)
        }
    }

    private func refresh() {
        viewModel.refresh()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NewsListView()
    }
}
