//
//  Dependencies.swift
//  NewsReader
//
//  Created by Robert Hartman on 8/6/21.
//

import Foundation

// Put singletons here
private struct Singletons {
    static let newsFetcher: NewsFetcher = NewsFetcherImpl(url: Constants.newsUrl)
}

// Each new dependency needs an entry here.
private struct Keys {
    struct NewsFetcherKey: InjectionKey {
        static var currentValue = Singletons.newsFetcher
    }

    struct NewsListViewModelKey: InjectionKey {
        // TODO: It seems that this would not be a singleton too. Would need to
        // figure out a way to conform to InjectionKey and create a new
        // instance when none exist, but disable the setter? Not sure...
        static var currentValue = NewsListViewModel()
    }
}

// Each new dependency needs an entry here, too.
extension InjectedValues {
    var newsFetcher: NewsFetcher {
        get { Self[Keys.NewsFetcherKey.self] }
        set { Self[Keys.NewsFetcherKey.self] = newValue }
    }

    var newsListViewModel: NewsListViewModel {
        get { Self[Keys.NewsListViewModelKey.self] }
        set { Self[Keys.NewsListViewModelKey.self] = newValue }
    }
}
